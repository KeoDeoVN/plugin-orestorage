package kdvn.plugin.sbd.main;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import kdvn.plugin.sbd.object.ConfigManager;
import kdvn.plugin.sbd.object.SBDManager;
import kdvn.plugin.sbd.storage.SBDStorage;

public class MainSBD extends JavaPlugin {
	
	public static MainSBD main;
	
	public FileConfiguration config;
	
	@Override
	public void onEnable() {
		main = this;
		this.saveDefaultConfig();
		this.reloadCF();
		
		Bukkit.getPluginManager().registerEvents(new EventHandling(), this);
		this.getCommand("orestorage").setExecutor(new Commands());
		this.getCommand("kho").setExecutor(new KhoCommand());
		
		Bukkit.getOnlinePlayers().forEach(p -> {
			SBDManager.load(p.getName());
		});
	}
	
	@Override
	public void onDisable() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			SBDManager.save(player.getName());
		});
	}
	
	public void reloadCF() {
		File file = new File(this.getDataFolder(), "config.yml");
		config = YamlConfiguration.loadConfiguration(file);
		ConfigManager.load(config);
		SBDStorage.init(config);
	}
	
	public void saveCF() {
		File file = new File(this.getDataFolder(), "config.yml");
		try {
			config.save(file);
		} catch (IOException e) {
		}
	}
}
